require 'net/http'
require 'uri'
require 'nokogiri'
require 'base64'
require 'securerandom'
require 'ruby-progressbar'

n = 500

#initialize a progressbar 
progressbar = ProgressBar.create(:format         => "%a %b\u{15E7}%i %p%% %t",
              :progress_mark  => ' ',
              :remainder_mark => "\u{FF65}",
              :starting_at    => 0,
              :total => n)

def grab_image
  uri = URI.parse("https://superxtar.com/wp-admin/admin-ajax.php")
  request = Net::HTTP::Post.new(uri)
  request.content_type = "application/x-www-form-urlencoded; charset=UTF-8"
  request["Cookie"] = "PHPSESSID=u389po11a0ms3guh5419kptbg4; pmpro_visit=1; 2wzuyv7=TPJiLeb7RS3x%2BHBChxcCQK3rcbTDkA%3D%3D; wp-settings-time-65907=1475960447; wordpress_test_cookie=WP+Cookie+check; _ga=GA1.2.2007701207.1475825087; wordfence_verifiedHuman=f8b82003d9e400d7093364d9f0388286; _gat=1; wfvt_4216992178=57f9d5c7b4c4e"
  request["Origin"] = "https://superxtar.com"
  request["Accept-Language"] = "en-US,en;q=0.8,ru;q=0.6"
  request["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"
  request["Accept"] = "*/*"
  request["Referer"] = "https://superxtar.com/"
  request["X-Requested-With"] = "XMLHttpRequest"
  request["Connection"] = "keep-alive"
  request.set_form_data(
    "action" => "cptch_reload",
    "cptch_nonce" => "42a2d48a75",
  )

  response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") do |http|
    http.request(request)
  end

  source = response.body
  page =  Nokogiri::HTML(source)

  page.css('span.cptch_span').each do |span|
    unless span.children.attr('src').nil?
      image_base64 = span.children.attr('src').text.split(',')[1]
      #write image to drive
      File.open("/Users/anton/Documents/projects/superxtar/images/#{SecureRandom.hex(4)}.png", "wb") do|f|
          f.write(Base64.decode64(image_base64))
          #puts "Image saved"
      end
    end
  end
end

n.times {grab_image; progressbar.increment}










