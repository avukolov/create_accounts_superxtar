require 'digest/md5'
require 'RMagick'
require 'net/http'
require 'uri'
require 'JSON'
require 'open-uri'
require 'nokogiri'
require 'base64'
require 'pg'
require 'randomuser'

$conn = PG.connect :host => 'localhost', :port => 5432,  :dbname => 'superxtar', :user => 'postgres', :password => 'password'

puts "How many accounts should be created?"
number = gets.chomp

def calculation(elements)
  if elements.index("x") == 0
      if elements[1] == "+"
        return res = elements[4].to_i - elements[2].to_i
        elsif elements[1] == "−"
            return res = elements[4].to_i + elements[2].to_i
      end
      elsif elements.index("x") == 2
        if elements[1] == "+"
          return res = elements[4].to_i - elements[0].to_i
          elsif elements[1] == "−"
            return res = (elements[4].to_i - elements[0].to_i) * -1
        end
      elsif elements.index("x") == 4
        if elements[1] == "+"
          return res = elements[0].to_i + elements[2].to_i
          elsif elements[1] == "−"
            return res = elements[0].to_i - elements[2].to_i
        end 
  end   
end

def create_account(email, username, password)
  uri = URI.parse("https://superxtar.com")
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  request = Net::HTTP::Get.new(uri.request_uri)
  response = http.request(request)

  res = Net::HTTP.get_response(uri)
  $cookie = res['set-cookie'].split('; ')[0]

  page =  Nokogiri::HTML(open('https://superxtar.com/'))
  elements = []

  page.css('span.cptch_span').children.each do |span|
    if span.attr('src').nil? || span.attr('src').empty?
      elements.push(span.text.gsub(/\u00a0/, ''))
    else 
      File.open("/Users/vukolovanton/Documents/projects/superxtar/images/temp.png", "wb") do|f|
        f.write(Base64.decode64(span.attr('src').split(',').last))
      end
      captcha_image = Magick::Image.read("/Users/vukolovanton/Documents/projects/superxtar/images/temp.png").first
      image_sum = Digest::MD5.hexdigest captcha_image.export_pixels.join
      elements.push($numbers.select {|x| x[1] == "#{image_sum}"}.flatten.first)
    end  
  end

  #replace array element to "x"
  elements[elements.index("")] = "x"
  elements.each_with_index do |elem, index| 
    if elem.to_s.length > 2
      elements[index] = $numbers.select {|x| x[1] == "#{elem}"}.flatten.first
    end
  end

  cptch_number = calculation(elements)
  cptch_result = page.css('input')[1].attr('value')
  cptch_time = page.css('input')[2].attr('value')

  uri = URI.parse("https://superxtar.com/wp-admin/admin-ajax.php")
  request = Net::HTTP::Post.new(uri)
  request.content_type = "application/x-www-form-urlencoded; charset=UTF-8"
  request["Cookie"] = $cookie
  request["Origin"] = "https://superxtar.com"
  request["Accept-Language"] = "en-US,en;q=0.8,ru;q=0.6"
  request["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"
  request["Accept"] = "*/*"
  request["Referer"] = "https://superxtar.com/"
  request["X-Requested-With"] = "XMLHttpRequest"
  request["Connection"] = "keep-alive"
  request.set_form_data(
    "user_login" => email,
    "user_email" => email,
    "nickname" => username,
    "user_password" => password,
    "user_password2" => password,
    "55fac7dd79894" => $countries.sample(1).first,
    "55fac8071130e[]" => "Yes",
    "cptch_number" => cptch_number,
    "cptch_result" => cptch_result,
    "cptch_time" => cptch_time,
    "zn_form_action" => "register",
    "action" => "ow_vote_pretty_login",
    "submit" => "http://superxtar.com/",
  )
  response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") do |http|
    http.request(request) 
  end
  response.code
  response.body
end

$countries = ["United States of America", "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia", "Bosnia Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Rep", "Chad", "Chile", "Republic of China", "Colombia", "Comoros", "Democratic Republic of the Congo", "Republic of the Congo", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Danzig", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gaza Strip", "The Gambia", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy Roman Empire", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Republic of Ireland", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jonathanland", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "North Korea", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mount Athos", "Mozambique", "Namibia", "Nauru", "Nepal", "Newfoundland", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Ottoman Empire", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Prussia", "Qatar", "Romania", "Rome", "Russian Federation", "Rwanda", "St Lucia", "Grenadines", "Samoa", "San Marino", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe" ]

$numbers = [[1, "one"], [2, "two"], [3, "three"], [4, "four"], [5, "five"], [6, "six"], [7, "seven"], [8, "eight"], [9, "nine"], [10, "ten"], [11, "eleven"], [12, "twelve"], [13, "thirteen"], [14, "fourteen"], [15, "fifteen"], [16, "sixteen"], [17, "seventeen"], [18, "eighteen"], [19, "nineteen"], [20, "twenty"], [0, "f1d5fd23c368ea88cdedfeb2e690d863"], [0, "d5a403726c4ab0c0c5ecd5940343ce9f"], [1, "f8469775660b217a15ea48372f49b0c1"], [1, "59b1779f9ea6c2c6d4f2e92c6a442270"], [2, "9e3698a379d2698811439d4b87d8e606"], [2, "a32e888b3bd55ec4bbf0eaa7901a6d2d"], [3, "f4249820eb9f7b2741ca07d3bbe8af8e"], [3, "5056d4df8820a1d760d6d9dd6f957005"], [4, "00ece32bd64ebab826e1897947fb51d8"], [4, "309cc1363495923c37f634c3579c9368"], [5, "081d9f25d3759e3f090edba393ba1768"], [5, "58cc64e81fca9b188f1917f541c2b008"], [6, "7eeba39fe2f75ab63d18392c85d26946"], [6, "519550508c479fbd1f9db8e3594e1fc6"], [7, "7ca2d83358fa06b6bd1c876f37a21b08"], [7, "5baa55925157f71b2ce85ce9aa2f823b"], [8, "9181e038848ab781aba64823b2d8fb0b"], [8, "fe33fab595bdcf136e9ba9fee3113640"], [9, "57f9dc91f3565d629e7d4b5bca7d555d"], [9, "fff0f34d99e6d2852c130d3d351e1b80"], [1, "228107bc8c5a1d6cd52f2a21f32ea8bf"], [1, "5f01a7f93c3d955ee49646c92785d816"], [2, "d05fa06a98e5228b13dcd0433b373277"], [2, "e8c546afa9e49cd9794e851b90c153ab"], [3, "89c915cd6ac42729387bc93918878277"], [3, "25f64815835ba8cf09dc81e204cf8084"], [4, "44ddf4c53d739d2d3eb80c8cf344d67d"], [4, "74bb5a38c0ef008c499fa7c3f08b4a20"], [5, "c3f9930e1ea94572a69e0c5f1f862fed"], [5, "1175c495f65d6c0d365bd0b976f61ebf"], [6, "052a9895dfe2c92dc54f8a6f7bb3ad23"], [6, "3501d57efb04ed912283d092bda972fd"], [7, "29c1abc41c5fc6b97d82b6b1f717b510"], [7, "e0f805d0c2ced1c61f9530433077191d"], [8, "f40db99af16cad159fdadcc48c1d9869"], [8, "065cd6af138bea0390c1a15c04bbcda0"], [9, "995fdffd760c980974ccbc4156f1f38a"], [9, "d349857b47ed67592312cf22ea1cb284"], [1, "86cec94fae6bb12cacaed70969076313"], [1, "489c2f601dd1544e71c85f5d81ee00c4"], [2, "cd5ef771a8f213c6b88782edfc15057b"], [2, "7c204db3f0b4bcaa365a34347b09c277"], [3, "a93ac9553444c1c17b77037114d03abf"], [3, "7b8d55ff65ad52dbd341a1b1f472de66"], [4, "1d1ac6a1769a3e73afbffd296d5fd1d4"], [4, "9b6b347d5e73875434dd0985f1811e48"], [5, "1f0f534c53cb7a687ec0a8a6c7bebf64"], [5, "62d258313622b2a241dd35144ba90e50"], [6, "b80a9c6c20efb30871a76e6b7525ce23"], [6, "782bfcaffdc8ae212ff276fa421a6c70"], [7, "bdff7160f0d8c0094765b569faadf5e3"], [7, "a0e29b623f1089bdaafdee919d7cc894"], [8, "c9a3d0b13b804af0653445fbd06bd273"], [8, "aba094dcea39b25017509f6aab5356ce"], [9, "fb19194f3dc02454e7bbe65fbdac7771"], [9, "76235760a6a55ecd581ebec829d188a0"]] 

results = $conn.query("SELECT email, password, username FROM voting WHERE created = FALSE LIMIT (#{number})")

results.each do |user|
  create_account(user['email'], user['username'], user['password'])
  $conn.query("UPDATE voting SET created = TRUE WHERE email = '#{user['email']}'")
  puts "#{user['email']}:#{user['password']}"
end



